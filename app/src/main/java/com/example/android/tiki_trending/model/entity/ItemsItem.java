package com.example.android.tiki_trending.model.entity;

import androidx.versionedparcelable.ParcelField;

import java.util.List;
import com.google.gson.annotations.SerializedName;
public class ItemsItem{

	@SerializedName("images")
	private List<String> images;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("title")
	private String title;

	public List<String> getImages(){
		return images;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public String getTitle(){
		return title;
	}
}