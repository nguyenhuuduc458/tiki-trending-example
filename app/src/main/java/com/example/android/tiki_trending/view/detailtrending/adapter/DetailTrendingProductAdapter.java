package com.example.android.tiki_trending.view.detailtrending.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.android.tiki_trending.R;
import com.example.android.tiki_trending.databinding.ItemDetailTrendingProductBinding;
import com.example.android.tiki_trending.model.entity.BadgesNewItem;
import com.example.android.tiki_trending.model.entity.DataItem;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;

public class DetailTrendingProductAdapter extends RecyclerView.Adapter<DetailTrendingProductAdapter.ViewHolder> {
    private List<DataItem> dataItemList;

    public DetailTrendingProductAdapter(List<DataItem> dataItemList) {
        this.dataItemList = dataItemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemDetailTrendingProductBinding binding = ItemDetailTrendingProductBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DataItem item = dataItemList.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return dataItemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ItemDetailTrendingProductBinding binding;

        public ViewHolder(ItemDetailTrendingProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(DataItem dataItem) {
            binding.setItem(dataItem);
            setBadgeItem(dataItem.getBadgesNew());
            setPrice(dataItem);
        }


        private void setBadgeItem(List<BadgesNewItem> badgesNewItems) {
            boolean isHaveBadgeTikiNow = false;
            boolean isHaveBadgePriceGuarantee = false;
            for (BadgesNewItem item : badgesNewItems) {
                if (item.getCode().equalsIgnoreCase("tikinow")) {
                    String icon = item.getIcon();
                    if (!icon.equalsIgnoreCase("r")) {
                        loadImage(binding.badge, icon);
                        isHaveBadgeTikiNow = true;
                    }
                } else if (item.getCode().equalsIgnoreCase("is_best_price_guaranteed")) {
                    String icon = item.getIcon();
                    if (!icon.equalsIgnoreCase("r")) {
                        loadImage(binding.bestPriceGuarantee, icon);
                        isHaveBadgePriceGuarantee = true;
                    }
                } else {
                    //
                }
            }
            if (!isHaveBadgePriceGuarantee) {
                binding.badge.setVisibility(View.INVISIBLE);
            }
            if (!isHaveBadgeTikiNow) {
                binding.bestPriceGuarantee.setVisibility(View.INVISIBLE);
            }
        }

        private void setPrice(DataItem item) {
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
            numberFormat.setMaximumFractionDigits(0);
            numberFormat.setCurrency(Currency.getInstance("VND"));
            binding.price.setText(numberFormat.format(item.getPrice()));

            int discountRate = item.getDiscountRate();
            if (discountRate > 0) {
                binding.price.setTextColor(Color.RED);
                binding.discountRate.setText("-" + discountRate + "%");
            } else {
                binding.price.setTextColor(Color.BLACK);
                binding.discountRate.setVisibility(View.GONE);
            }
        }


        private void loadImage(ImageView imageView, String url) {
            Glide.with(imageView.getContext())
                    .load(url)
                    .placeholder(R.drawable.error_image)
                    .error(R.drawable.error_image)
                    .into(imageView);
        }
    }


}
