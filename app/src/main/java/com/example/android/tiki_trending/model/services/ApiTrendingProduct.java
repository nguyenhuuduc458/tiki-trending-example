package com.example.android.tiki_trending.model.services;


import com.example.android.tiki_trending.model.entity.Response;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiTrendingProduct {

    @GET("shopping-trend/api/trendings/hub?")
    Call<Response> getCategory(
            @Query("cursor") int cursor,
            @Query("limit") int limit);

    @GET("shopping-trend/api/trendings/hub/category_id/{id}?")
    Call<Response> getProduct(
        @Path("id") int id,
        @Query("cursor") int cursor,
        @Query("limit") int limit
    );
}
