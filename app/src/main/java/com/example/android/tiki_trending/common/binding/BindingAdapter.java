package com.example.android.tiki_trending.common.binding;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.example.android.tiki_trending.R;
import com.jgabrielfreitas.core.BlurImageView;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class BindingAdapter {
    @androidx.databinding.BindingAdapter("imageUrl")
    public static void loadImage(ImageView image, String url) {
        Glide.with(image.getContext())
                .load(url)
                .placeholder(R.drawable.error_image)
                .error(R.drawable.error_image)
                .into(image);
    }

    @androidx.databinding.BindingAdapter("roundImageUrl")
    public static void loadRoundImage(RoundedImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.error_image)
                .error(R.drawable.error_image)
                .into(imageView);
    }

    @androidx.databinding.BindingAdapter("blurImageUrl")
    public static void loadBackgroundImage(BlurImageView image, String url) {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            try {
                Bitmap bitmap = Glide.with(image.getContext())
                        .asBitmap()
                        .placeholder(R.drawable.error_image)
                        .error(R.drawable.error_image)
                        .load(url)
                        .submit().get();
                new Handler(Looper.getMainLooper()).post(() -> {
                    image.setImageBitmap(bitmap);
                    image.setBlur(10);
                });
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    @androidx.databinding.BindingAdapter("isVisible")
    public static void setVisibility(View view, boolean isVisible) {
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.INVISIBLE);
        }
    }

    @androidx.databinding.BindingAdapter("ratingValue")
    public static void setRating(RatingBar view, String rating) {
        if (view != null) {
            float rate = Float.parseFloat(rating);
            view.setRating(rate);
        }
    }
}
