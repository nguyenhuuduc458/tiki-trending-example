package com.example.android.tiki_trending.model.repositories;

import com.example.android.tiki_trending.model.entity.Response;
import com.example.android.tiki_trending.model.services.ApiTrendingProduct;
import com.example.android.tiki_trending.model.services.ResultData;
import com.example.android.tiki_trending.model.services.RetrofitBuilder;

public class TrendingProductRepository extends BaseRepository<Response> {
    public void getCategory(int cursor, int limit, ResultData resultData) {
        ApiTrendingProduct service = RetrofitBuilder.getClient(ApiTrendingProduct.class);
        process(service.getCategory(cursor, limit), (data, t) -> {
            if (data != null) {
                resultData.invoke(data);
            } else {
               resultData.invoke(null);
            }
        });
    }

    public void getProductInCategory(int id, int cursor, int limit, ResultData resultData) {
        ApiTrendingProduct service = RetrofitBuilder.getClient(ApiTrendingProduct.class);
        process(service.getProduct(id, cursor, limit), (data, t) -> {
            if (data != null) {
                resultData.invoke(data);
            } else {
                resultData.invoke(null);
            }
        });
    }
}



