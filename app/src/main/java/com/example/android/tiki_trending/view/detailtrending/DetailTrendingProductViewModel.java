package com.example.android.tiki_trending.view.detailtrending;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.android.tiki_trending.model.entity.DataItem;
import com.example.android.tiki_trending.model.repositories.TrendingProductRepository;
import com.example.android.tiki_trending.view.trendingproduct.model.CategoryModel;

import java.util.List;

public class DetailTrendingProductViewModel extends ViewModel {
    private MutableLiveData<CategoryModel> categoryModel = new MutableLiveData<CategoryModel>();
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>(true);
    MutableLiveData<List<DataItem>> products = new MutableLiveData<>();

    public DetailTrendingProductViewModel(CategoryModel categoryModel) {
        this.categoryModel.setValue(categoryModel);
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public MutableLiveData<CategoryModel> getCategoryModel() {
        return categoryModel;
    }

    public MutableLiveData<List<DataItem>> getProducts() {
        return products;
    }

    public void loadData(int categoryId, int cursor, int limit) {
        isLoading.setValue(true);
        TrendingProductRepository productRepository = new TrendingProductRepository();
        productRepository.getProductInCategory(categoryId, cursor, limit, (data) -> {
            if (data == null) return;
            products.setValue(data.getData().getData());
            isLoading.setValue(false);
        });
    }

    public void registerSwipeToRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            loadData(categoryModel.getValue().getItem().getCategoryId(), categoryModel.getValue().getCursor(), 50);
            swipeRefreshLayout.setRefreshing(false);
        });
    }
}

class DetailTrendingProductViewModelFactory implements ViewModelProvider.Factory {
    private CategoryModel categoryModel;

    public DetailTrendingProductViewModelFactory(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(DetailTrendingProductViewModel.class)) {
            return (T) new DetailTrendingProductViewModel(categoryModel);
        }
        throw new IllegalArgumentException("Argument is invalid");
    }
}