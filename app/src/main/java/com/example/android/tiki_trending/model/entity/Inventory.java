package com.example.android.tiki_trending.model.entity;

import com.google.gson.annotations.SerializedName;

public class Inventory{

	@SerializedName("fulfillment_type")
	private String fulfillmentType;

	public String getFulfillmentType(){
		return fulfillmentType;
	}
}