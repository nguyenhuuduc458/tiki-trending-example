package com.example.android.tiki_trending.view.detailtrending;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.android.tiki_trending.R;
import com.example.android.tiki_trending.databinding.ActivityDetailTrendingProductBinding;
import com.example.android.tiki_trending.view.detailtrending.adapter.DetailTrendingProductAdapter;
import com.example.android.tiki_trending.view.trendingproduct.model.CategoryModel;
import com.google.gson.Gson;

public class DetailTrendingProductActivity extends AppCompatActivity {

    private ActivityDetailTrendingProductBinding binding;
    private DetailTrendingProductViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_trending_product);

        setupActionBar();
        Intent intent = getIntent();
        Gson gson = new Gson();
        CategoryModel item = gson.fromJson(intent.getStringExtra("category"), CategoryModel.class);
        DetailTrendingProductViewModelFactory factory = new DetailTrendingProductViewModelFactory(item);
        viewModel = new ViewModelProvider(this, factory).get(DetailTrendingProductViewModel.class);

        viewModel.getCategoryModel().observe(this, categoryModel -> {
            viewModel.loadData(categoryModel.getItem().getCategoryId(), categoryModel.getCursor(), 50);
            binding.toolBar.setTitle(categoryModel.getItem().getTitle());
        });

        viewModel.getProducts().observe(this, dataItemList -> {
            binding.detailTrendingProduct.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            DetailTrendingProductAdapter adapter = new DetailTrendingProductAdapter(dataItemList);
            binding.detailTrendingProduct.setAdapter(adapter);

        });
        viewModel.registerSwipeToRefreshLayout(binding.swipeRefreshDetailTrendingProduct);
    }

    private void setupActionBar(){
        binding.toolBar.setNavigationOnClickListener(view -> finish());
    }
}
