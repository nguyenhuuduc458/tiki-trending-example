package com.example.android.tiki_trending.model.services;


import com.example.android.tiki_trending.model.entity.Response;

import java.util.List;

public interface ResultData {
    void invoke(Response responses);
}
