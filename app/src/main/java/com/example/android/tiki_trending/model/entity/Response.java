package com.example.android.tiki_trending.model.entity;

import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("data")
	private Data data;

	@SerializedName("status")
	private int status;

	public Data getData(){
		return data;
	}

	public int getStatus(){
		return status;
	}
}