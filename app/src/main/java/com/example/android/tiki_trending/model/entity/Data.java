package com.example.android.tiki_trending.model.entity;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("next_page")
	private String nextPage;

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("meta_data")
	private MetaData metaData;

	public String getNextPage(){
		return nextPage;
	}

	public List<DataItem> getData(){
		return data;
	}

	public MetaData getMetaData(){
		return metaData;
	}
}