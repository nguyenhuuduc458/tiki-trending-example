# Tiki-trending

### Prerequisites

- Basic knowledge of android components
- Have a basic knowledge of the android architecture components
- Familiar with `Android studio IDE`
- Using Java programming language
- Using MVVM architecture
- Using library like: 
    + Retrofit (Request data from api)
    + Glide (Load image from internet)
    + Material designs

### Clone

- Clone this repository to your local machine using  `https://gitlab.com/nguyenhuuduc458/tiki-trending-example.git`

### Maintainer
> Nguyễn Hữu Đức 

### Git Flow
- Step 1: Checkout developer branch: `git checkout develop`
- Step 2: Create your feature branch: `git checkout -b feature/{feature_name}`
- Step 3: After you finish your task checkout to develop and merge your feature branch option --no-ff when you want to keep history of branch : `git merge --no--ff {branch_name}`
- Step 4: Remove your old branch: `git branch -d {branch_name}`
- Step 5: Commit branch to git: `git commit -m "somthing"`
- Step 5: Push branch: `git push origin develope`

### License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2015 © <a href="http://fvcproductions.com" target="_blank">FVCproductions</a>.
